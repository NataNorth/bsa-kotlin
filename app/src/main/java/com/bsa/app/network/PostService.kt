package com.bsa.app.network

import com.bsa.app.database.Post
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface PostService {
    @GET("posts")
    suspend fun getPosts() : Deferred<List<Post>>

//    companion object {
//        private const val BASE_URL = "http://jsonplaceholder.typicode.com/"
//
//        fun create(): PostService {
//            val logger = HttpLoggingInterceptor().apply { level = Level.BASIC }
//
//            val client = OkHttpClient.Builder()
//                .addInterceptor(logger)
//                .build()
//
//            return Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//                .create(PostService::class.java)
//        }
//    }
}

object PostNetwork {

    private const val BASE_URL = "http://jsonplaceholder.typicode.com/"

    val client = OkHttpClient.Builder()
                    .build()

    // Configure retrofit to parse JSON and use coroutines
    private val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

    val posts = retrofit.create(PostService::class.java)

}

