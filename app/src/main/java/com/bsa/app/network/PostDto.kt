package com.bsa.app.network

import com.bsa.app.database.Post
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiPost(
    val userId: String,
    val id: String,
    val title: String,
    val body: String )


fun List<ApiPost>.asDatabaseModel(): List<Post> {
    return map{
        Post(
            id = it.id,
            title = it.title,
            body = it.body,
            userId = it.userId
        )
    }
}