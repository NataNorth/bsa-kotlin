package com.bsa.app.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bsa.app.database.AppDatabase
import com.bsa.app.database.PostDao
import com.bsa.app.database.asDomainModel
import com.bsa.app.domain.Post
import com.bsa.app.network.PostNetwork
import com.bsa.app.network.PostService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class PostRepository internal constructor(private val appDatabase: AppDatabase) {

    val posts: LiveData<List<Post>> = Transformations.map(appDatabase.postDao.getPosts()) {
        it.asDomainModel()
    }

//    fun getPosts() = postDao.getPosts()
//
//    fun getPost(postId: String) = postDao.getPost(postId)
//
//    companion object {
//
//        // For Singleton instantiation
//        @Volatile private var instance: PostRepository? = null
//
//        fun getInstance(postDao: PostDao) =
//            instance ?: synchronized(this) {
//                instance
//                    ?: PostRepository(postDao)
//                        .also { instance = it }
//            }
//    }

    suspend fun refreshPosts() {
        withContext(Dispatchers.IO) {
            Timber.d("refresh posts is called");
            val posts = PostNetwork.posts.getPosts().await()
//            appDatabase.postDao.insertAll(posts)
        }
    }
}