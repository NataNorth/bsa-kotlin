package com.bsa.app.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PostDao {
    @Transaction
    @Query("SELECT * FROM posts")
    fun getPosts(): LiveData<List<PostWithComments>>

    @Transaction
    @Query("SELECT * FROM posts WHERE id = :postId")
    fun getPost(postId: String): LiveData<PostWithComments>

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insertAll(posts: List<Post>)
}

@Database(entities = [Post::class, Comment::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){
    abstract val postDao: PostDao
}

private lateinit var INSTANCE: AppDatabase

fun getDatabase(context: Context): AppDatabase {
    synchronized(AppDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                AppDatabase::class.java,
                "posts").build()
        }
    }
    return INSTANCE
}
