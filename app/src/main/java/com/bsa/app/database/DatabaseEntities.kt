package com.bsa.app.database

import androidx.room.*

@Entity(tableName = "posts")
data class Post(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    val title: String,
    val body: String,
    val userId: String
)

@Entity(tableName = "comments")
data class Comment(
    @PrimaryKey @ColumnInfo(name="id") val id: String,
    val text: String,
    val postId: String
)

data class PostWithComments(
    @Embedded val post: Post,
    @Relation(
        parentColumn = "id",
        entityColumn = "postId"
    )
    val comments: List<Comment>
)

@Entity(tableName = "users")
data class User(
    @PrimaryKey @ColumnInfo(name="id") val id: String,
    val name: String
)

//data class PostWithCommentsAndUser(
//    @Embedded val postWithComments: PostWithComments,
//    @Relation(
//        entity = Post::class,
//        parentColumn = "id",
//        entityColumn = "userId"
//    )
//    val user: User
//)

fun List<PostWithComments>.asDomainModel(): List<com.bsa.app.domain.Post> {
    return map {
        com.bsa.app.domain.Post(
            title = it.post.title,
            body = it.post.body,
            userName = "",
            comments = it.comments
        )
    }
}



