package com.bsa.app.domain

import com.bsa.app.database.Comment

data class Post(var title: String,
                var body: String,
                var userName: String,
                var comments: List<Comment>)
